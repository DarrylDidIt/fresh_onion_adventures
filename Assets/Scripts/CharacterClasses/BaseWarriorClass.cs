﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWarriorClass : BaseCharacterClass {

	public  BaseWarriorClass() {
		CharacterClassName = "Warrior";
		CharacterClassDescription = "A Strong and powerful Hero";

		//Chacrtater Stats
		Accuracy = 10; 
		Strength = 19;
		Magic = 3;
		Speed = 9;
		PhysicalDefence = 17;
		MagicalDefence = 10;

 	}
 }
