﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BaseCharacterClass {

	private string characterClassName;
	private string characterClassDescription;
	
	//stats
	private int accuracy; 
	private int strength;
	private int magic;
	private int speed;
	private int physicalDefence;
	private int magicalDefence;

	public string CharacterClassName {
		get{ return characterClassName; }
		set{ characterClassName = value; }
	}

	public string CharacterClassDescription {get;set;}
	public int Accuracy {get;set;}
	public int Strength {get;set;}
	public int Magic {get;set;}
	public int Speed {get;set;}
	public int PhysicalDefence {get;set;}
	public int MagicalDefence {get;set;}

}
