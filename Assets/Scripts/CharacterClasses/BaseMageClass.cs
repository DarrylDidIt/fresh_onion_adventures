﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMageClass : BaseCharacterClass {

	public BaseMageClass() {
		CharacterClassName = "Mage";
		CharacterClassDescription = "A Strong and powerful Hero";

		//Chacrtater Stats
		Accuracy = 15; 
		Strength = 5;
		Magic = 20;
		Speed = 14;
		PhysicalDefence = 9;
		MagicalDefence = 14;
 	}
 }
