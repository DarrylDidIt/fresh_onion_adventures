﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadInformation {

	public static void LoadAllInformation() {
		GameInformation.PlayerName = PlayerPrefs.GetString("PLayer Name");
		GameInformation.PlayerLevel = PlayerPrefs.GetInt("Player Level");
		GameInformation.Accuracy = PlayerPrefs.GetInt("Accuracy");
		GameInformation.Strength = PlayerPrefs.GetInt("Strength");
		GameInformation.Magic = PlayerPrefs.GetInt("Magic");
		GameInformation.PhysicalDefence = PlayerPrefs.GetInt("Physical Defence");
		GameInformation.MagicalDefence = PlayerPrefs.GetInt("Magical Defence");

		if (PlayerPrefs.GetString("Equipment Item1") != null)
		{
			GameInformation.EquipmentOne = (BaseEquipment)PPSerialization.Load("Equipment Item1");
		}
	}
	
}
