﻿using System.Collections;
using UnityEngine;

public class SaveInformation {

	public static void SaveAllInformation() {
		PlayerPrefs.SetInt("Player Level", GameInformation.PlayerLevel);
		PlayerPrefs.SetString("PLayer Name", GameInformation.PlayerName);
		PlayerPrefs.SetInt("Accuracy", GameInformation.Accuracy);
		PlayerPrefs.SetInt("Strength", GameInformation.Strength);
		PlayerPrefs.SetInt("Magic", GameInformation.Magic);
		PlayerPrefs.SetInt("Physical Defence", GameInformation.PhysicalDefence);
		PlayerPrefs.SetInt("Magical Defence", GameInformation.MagicalDefence);

		if (GameInformation.EquipmentOne != null)
		{
			PPSerialization.Save("Equipment Item1", GameInformation.EquipmentOne);
		}
		
		Debug.Log("All Information Saved");
	}
}
