﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInformation : MonoBehaviour {

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
	}

	public static BaseEquipment EquipmentOne {get;set;}
	public static string PlayerName{get;set;}
	public static int PlayerLevel{get;set;}
	public static BaseCharacterClass PlayerClass {get;set;}
	public static int Accuracy{get;set;}
	public static int Strength{get;set;}
	public static int Magic{get;set;}
	public static int Speed{get;set;}
	public static int PhysicalDefence{get;set;}
	public static int MagicalDefence{get;set;}
}
