﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePlayer {

	private string playerName;
	private int playerLevel;
	private BaseCharacterClass playerClass;
	private int accuracy; 
	private int strength;
	private int magic;
	private int speed;
	private int physicalDefence;
	private int magicalDefence;

	public string PlayerName{get;set;}

	// Player Level
	public int PlayerLevel{get;set;}

	//Player Class
	public BaseCharacterClass PlayerClass{get;set;}

	//Player Accuracy
	public int Accuracy{get;set;}

	//Player Strength
	public int Strength{get;set;}

	//Player Magic
	public int Magic{get;set;}

	//Player Speen
	public int Speed{get;set;}

	//Player Phy. Defence
	public int PhysicalDefence{get;set;}

	//Player Mag. Defence
	public int MagicalDefence{get;set;}
}
