﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePotion : BaseStatItem {

	public enum PotionTypes {
		HEALTH,
		MANA,
		REVIVE
	}

	private PotionTypes potionType;
	private int spellEffectID;

	public PotionTypes PotionType {get;set;}
	public int SpellEffectID {get;set;}
}
