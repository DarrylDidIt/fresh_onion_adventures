﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWeapon : BaseStatItem {

	public enum WeaponTypes {
		SWORD,
		WAND,
		DAGGER,
		GUN,
		SHEILD
	}

	private WeaponTypes weaponType;
	private int spellEffectID;
 	
	 public WeaponTypes WeaponType {get;set;}
	
	public int SpellEffectID {get;set;}
}
