﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		LoadInformation.LoadAllInformation();
		Debug.Log("Player Name: " + GameInformation.PlayerName);
		Debug.Log("Player Level: " + GameInformation.PlayerLevel);
		Debug.Log("Player Accuracy: " + GameInformation.Accuracy);
		Debug.Log("Player Strength: " + GameInformation.Strength);
		Debug.Log("Player Magic: " + GameInformation.Magic);
		Debug.Log("Player Speed: " + GameInformation.Speed);
		Debug.Log("Player PhysicalDefence: " + GameInformation.PhysicalDefence);
		Debug.Log("Player MagicalDefence: " + GameInformation.MagicalDefence);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
