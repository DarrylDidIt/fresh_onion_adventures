﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnBaseCombatSystem : MonoBehaviour {

	public enum BattleStates {
		// PLayer Animate,
		START,
		//Story Dialogue,
		PLAYERCHOICE,
		ENEMYCHOICE,
		LOSE,
		WIN
	}

	private BattleStates currentState;

	// Use this for initialization
	void Start () {
		currentState = BattleStates.START;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(currentState);
		switch (currentState) {
			case (BattleStates.START):
				//STEUP BATTLE FUNCTION
				break;
			case (BattleStates.PLAYERCHOICE):
				break; 
			case (BattleStates.ENEMYCHOICE):
				break;
			case (BattleStates.LOSE):
				break; 
			case (BattleStates.WIN):
				break; 
		}
	}

	void OnGUI() {
		if(GUILayout.Button("Battle Start!")) {
			if  (currentState == BattleStates.START){
				currentState = BattleStates.PLAYERCHOICE;
			} else if (currentState == BattleStates.PLAYERCHOICE) {
				currentState = BattleStates.ENEMYCHOICE;
			} else if (currentState == BattleStates.ENEMYCHOICE) {
				currentState = BattleStates.LOSE;
			} else if (currentState == BattleStates.LOSE) {
				currentState = BattleStates.WIN;
			} else if (currentState == BattleStates.WIN) {
				currentState = BattleStates.START;
			}
		}
	}
}
