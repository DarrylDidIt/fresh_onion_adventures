﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CreateNewCharacter : MonoBehaviour {

	private BasePlayer newPlayer;
	private bool isMageClass;
	private bool isWarriorClass;
	private string playerName = "Enter Name";

	// Use this for initialization
	void Start () {
		newPlayer = new BasePlayer();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI() {
		playerName = GUILayout.TextArea(playerName);
		isMageClass = GUILayout.Toggle(isMageClass, "Mage");
		isWarriorClass = GUILayout.Toggle(isWarriorClass, "Warrior");

		if(GUILayout.Button("Create")){
			if(isMageClass) {
				newPlayer.PlayerClass = new BaseMageClass();
			}else if (isWarriorClass){
				newPlayer.PlayerClass = new BaseWarriorClass();
			}
			newPlayer.PlayerLevel = 1;
			newPlayer.Accuracy = newPlayer.PlayerClass.Accuracy;
			newPlayer.Strength = newPlayer.PlayerClass.Strength;
			newPlayer.Magic = newPlayer.PlayerClass.Magic;
			newPlayer.Speed = newPlayer.PlayerClass.Speed;
			newPlayer.PhysicalDefence = newPlayer.PlayerClass.PhysicalDefence;
			newPlayer.MagicalDefence = newPlayer.PlayerClass.MagicalDefence;
			newPlayer.PlayerName = playerName;
			StoreNewPlayerInfo();
			SaveInformation.SaveAllInformation();
			Debug.Log("Player Name:" + newPlayer.PlayerName);
			Debug.Log("Player Class:" + newPlayer.PlayerClass.CharacterClassName);
			Debug.Log("Player Level:" + newPlayer.PlayerLevel);
			Debug.Log("Player Accuracy:" + newPlayer.Accuracy);
			Debug.Log("Player Strength:" + newPlayer.Strength);
			Debug.Log("Player Magic:" + newPlayer.Magic);
			Debug.Log("Player Speed:" + newPlayer.Speed);
			Debug.Log("Player Physical Defence:" + newPlayer.PhysicalDefence);
			Debug.Log("Player Magical Defence:" + newPlayer.MagicalDefence);
		}

		if(GUILayout.Button("Load")){
			SceneManager.LoadScene("Test");
		}
	}

	private void StoreNewPlayerInfo() {
		GameInformation.PlayerName = newPlayer.PlayerName;
		GameInformation.PlayerLevel = newPlayer.PlayerLevel;
		GameInformation.Accuracy = newPlayer.Accuracy;
		GameInformation.Strength = newPlayer.Strength;
		GameInformation.Magic = newPlayer.Magic;
		GameInformation.Speed = newPlayer.Speed;
		GameInformation.PhysicalDefence = newPlayer.PhysicalDefence;
		GameInformation.MagicalDefence = newPlayer.MagicalDefence;
	}
}
