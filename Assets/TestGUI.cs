﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGUI : MonoBehaviour {

	private BaseCharacterClass class1 = new BaseMageClass();
	private BaseCharacterClass class2 = new BaseWarriorClass();


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI () {
		//Mage Character Stats
		GUILayout.Label( class1.CharacterClassName );
		GUILayout.Label( class1.CharacterClassDescription );
		GUILayout.Label( class1.Accuracy.ToString() );
		GUILayout.Label( class1.Strength.ToString() );
		GUILayout.Label( class1.Magic.ToString() );
		GUILayout.Label( class1.Speed.ToString() );
		GUILayout.Label( class1.PhysicalDefence.ToString() );
		GUILayout.Label( class1.MagicalDefence.ToString() );

		//Warriors Character Stats
		GUILayout.Label( class2.CharacterClassName );
		GUILayout.Label( class2.CharacterClassDescription );
		GUILayout.Label( class2.Accuracy.ToString() );
		GUILayout.Label( class2.Strength.ToString() );
		GUILayout.Label( class2.Magic.ToString() );
		GUILayout.Label( class2.Speed.ToString() );
		GUILayout.Label( class2.PhysicalDefence.ToString() );
		GUILayout.Label( class2.MagicalDefence.ToString() );


 

	}
}
